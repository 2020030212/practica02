package com.example.practica02_imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnSalir;
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblIMC = (TextView) findViewById(R.id.lblIMC);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = txtAltura.getText().toString();
                String num2 = txtPeso.getText().toString();
                Double res = 0.0;

                if (txtAltura.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Faltó capturar información", Toast.LENGTH_SHORT).show();
                } else if (txtPeso.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Faltó capturar información", Toast.LENGTH_SHORT).show();
                } else {
                    Double altura = Double.parseDouble(num1);
                    Double peso = Double.parseDouble(num2);
                    res= peso / (altura * altura);
                    double resultado = Math.round(res * 10.0)/10.0;

                    lblIMC.setText("El IMC es: " + resultado);
                }

            }
        });


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblIMC.setText("");
            }
        });
    }
}